(function () {

    function Clock() {

        var minutes = 0;
        var seconds = 0;

        function increment_minutes() {
            minutes = minutes + 1;
            seconds = 0;
        }

        function increment_seconds() {
            seconds = seconds + 1;
        }

        return {
            increment: function () {
                if (seconds === 59) {
                    increment_minutes();
                } else {
                    increment_seconds();
                }
            },
            render: function () {
                var result = '';
                var m = minutes.toString();
                if (m.length === 1) {
                    result = result + '0' + m;
                } else {
                    result = result + m;
                }

                result = result + ':';

                var s = seconds.toString();
                if (s.length === 1) {
                    result = result + '0' + s;
                } else {
                    result = result + s;
                }

                return result;
            }
        }
    }

    var app = angular.module('footballGuiApp', []);

    function incidentFilter(incidentName) {
        return function (items) {
            var filtered = [];
            items.forEach(function(item) {
                if (item.name === incidentName) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }

    app.filter('homeYellowFilter', function () {
        return incidentFilter('Home Yellow');
    });

    app.filter('homeRedFilter', function () {
        return incidentFilter('Home Red');
    });

    app.filter('awayYellowFilter', function () {
        return incidentFilter('Away Yellow');
    });

    app.filter('awayRedFilter', function () {
        return incidentFilter('Away Red');
    });

    app.controller('matchCtrl', ['$scope', '$interval', function ($scope, $interval) {

        var clock = Clock();

        $scope.state = {
            time: clock.render(),
            homeScore: 0,
            awayScore: 0,
            homeReds: 0,
            awayReds: 0,
            homeYellows: 0,
            awayYellows: 0,
            incidents: []
        };

        function addIncident(name) {
            $scope.state.incidents.unshift({
                time: clock.render(),
                name: name
            });
        }

        $interval(function () {
            clock.increment();

            $scope.state.time = clock.render();
        }, 1000);

        $scope.homeGoal = function() {
            addIncident('Home Goal');
            $scope.state.homeScore++;
        };

        $scope.awayGoal = function() {
            addIncident('Away Goal');
            $scope.state.awayScore++;
        };

        $scope.homeRed = function() {
            addIncident('Home Red');
            $scope.state.homeReds++;
        };

        $scope.awayRed = function() {
            addIncident('Away Red');
            $scope.state.awayReds++;
        };

        $scope.homeYellow = function() {
            addIncident('Home Yellow');
            $scope.state.homeYellows++;
        };

        $scope.awayYellow = function() {
            addIncident('Away Yellow');
            $scope.state.awayYellows++;
        };

        // filters
        $scope.incidentFilter = function(incident, name) {
            return incident.name === name;
        }
    }]);

    app.controller('guiController', ['$scope', function ($scope) {

        $scope.matches = [];

        $scope.matchId = '';

        $scope.addMatch = function () {
            var id = $scope.matchId;

            if ($scope.matches.length !== 3) {
                $scope.matches.push(id);
            }
        }
    }]);

})();